#include <iostream>
#include <string>

using namespace std;


typedef int Metros;

struct persona{
    string nombre;
    int edad;
    long numeroEmpleado;
};

struct Fecha{
    int dia;
    char mes[10];
    int anio;
};

void imprimePersona(struct persona);

int main () {
    // definir
    struct persona Persona1;
    
    Metros cantidad;
    cantidad = 10;
    
    // asignar valres
    Persona1.edad = 40;
    Persona1.nombre = "David";
    Persona1.numeroEmpleado = 93939;
    
    // asignar valores al definir
    struct persona Persona2 = {"Jhon", 15, 838383};
    
    imprimePersona(Persona1);
    imprimePersona(Persona2);
    
    struct persona Personas[10];
    
    Personas[0].nombre = "Jane";
    Personas[1].nombre = "Peter";
    Personas[2].nombre = "Ricardo";
    
    cout << "Nombre: " << Personas[2].nombre << endl;
    
    struct Fecha x;
    struct Fecha *hoy = &x;
    /*
                              dia        mes        anio
     hoy -------->  0x010 ..[     ]  [           ] [    ]
     */
    int num;
    int *numero = &num;
    *numero = 10;
    cout << "El numero apuntado es: " << *numero << endl;
    
    hoy->anio = 1987;
    (*hoy).anio = 1900;
    
    strcpy(hoy->mes, "Septiembre");
    hoy->dia = 21;
    
    cout << hoy->dia << "-" << hoy->mes << "-" << hoy->anio << endl;
    
    return 0;
}

void imprimePersona(struct persona Persona){
    cout << "Nombre: " << Persona.nombre << endl;
    cout << "Edad: " << Persona.edad << endl;
    cout << "Numero Empleado: " << Persona.numeroEmpleado << endl;
}


// david@olmo.mx

